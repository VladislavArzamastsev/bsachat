import * as React from 'react';
import './messageInput.css';
import PropTypes from 'prop-types';

class MessageInput extends React.Component {

    sendMessage() {
        const messageInput = document.getElementsByClassName("message-input-text")[0];
        const messageIdHolder = document.getElementById("editedMessageIdHolder");
        const message = messageInput.value;
        this.props.onMessageSend(message, messageIdHolder.value);
        messageInput.value = "";
        messageIdHolder.value = "";
    }

    render() {
        return (
            <div className="message-input">
                <input className="message-input-text">

                </input>
                <input id="editedMessageIdHolder" type="hidden"/>
                <button
                    className="message-input-button"
                    onClick={() => this.sendMessage()}
                >
                    Send
                </button>
            </div>
        );
    }

}

MessageInput.propTypes = {
    onMessageSend: PropTypes.func
}

export default MessageInput;
