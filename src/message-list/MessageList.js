import * as React from 'react';
import "./messageList.css";
import PropTypes from 'prop-types';
import OwnMessage from "../message/OwnMessage";
import Message from "../message/Message";
import {compareDatesWithoutTime} from "../utils/date-time-utils";
import {v4 as uuidv4} from 'uuid';

class MessageList extends React.Component {

    constructor(props) {
        super(props);
        this.currentUserId = props.currentUserId;
    }

    mapMessageToAppropriateComponent(m) {
        if (m.userId === this.currentUserId) {
            return <OwnMessage
                key={uuidv4()}
                message={m}
                onDelete={this.props.onDelete}
                onUpdate={this.props.onUpdate}
            />;
        }
        return (
            <Message
                key={uuidv4()}
                liked = {this.props.likedMessagesIds.has(m.id)}
                message={m}
                onLike={this.props.onLike}
            />);
    }

    getMessageDivider(date) {
        let dividerMessage;
        const today = new Date();
        const yesterday = new Date().setDate(today.getDate() - 1);
        if (compareDatesWithoutTime(date, today) === 0) {
            dividerMessage = "Today";
        } else if (compareDatesWithoutTime(date, yesterday) === 0) {
            dividerMessage = "Yesterday";
        } else {
            const day = date.getDate();
            const longMonth = date.toLocaleString('en-us', {month: 'long'});
            const weekday = new Intl.DateTimeFormat('en-US', {weekday: 'long'}).format(date);
            dividerMessage = weekday + ", " + day + " " + longMonth;
        }
        return (
            <div
                key={uuidv4()}
                className="messages-divider"
            >{dividerMessage}
            </div>
        );
    }

    createMessageList(messages) {
        const out = [];
        let previous = undefined;
        let current = undefined;
        for (let i = 0; i < messages.length; i++) {
            current = messages[i];
            if (previous === undefined ||
                (i < messages.length && compareDatesWithoutTime(current.createdAt, previous.createdAt) > 0)) {
                out.push(this.getMessageDivider(current.createdAt));
            }
            out.push(this.mapMessageToAppropriateComponent(current));
            previous = current;
        }
        return out;
    }

    render() {
        return (
            <div className="message-list">
                {this.createMessageList(this.props.messages)}
            </div>
        );
    }
}

MessageList.propTypes = {
    messages: PropTypes.array,
    currentUserId: PropTypes.string,
    onDelete: PropTypes.func,
    onLike: PropTypes.func,
    onUpdate: PropTypes.func,
    likedMessagesIds: PropTypes.any
}

export default MessageList;