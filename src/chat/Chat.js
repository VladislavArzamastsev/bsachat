import * as React from 'react';
import "./chat.css";
import Header from '../header/Header';
import MessageEntity from "../entity/MessageEntity";
import PropTypes from 'prop-types';
import MessageList from "../message-list/MessageList";
import MessageInput from "../message-input/MessageInput";
import Preloader from "../preloader/Preloader";
import {v4 as uuidv4} from 'uuid';


class Chat extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            chatName: "My chat",
            participantsCount: 0,
            lastMessageDate: undefined,
            isLoading: true
        };
        this.likedMessagesIds = new Set();
    }

    onMessageSend = (messageText, messageId) => {
        if (messageText === undefined || messageText === "") {
            return;
        }
        if (messageId === undefined || messageId === "") {
            const id = uuidv4();
            const now = new Date();
            const entity = new MessageEntity(id,
                this.props.currentUserId, this.props.currentAvatar, this.props.currentUserName,
                messageText, now, undefined, 0);
            const newMessages = [...this.state.messages, entity];
            this.setState({
                messages: newMessages,
                lastMessageDate: now,
                participantsCount: new Set(newMessages.map(m => m.userId)).size
            });
        } else {
            this.setState({
                    messages: this.state.messages.map(m => m.id !== messageId ? m : {
                        ...m,
                        text: messageText,
                        editedAt: new Date()
                    })
                }
            );
        }
    }

    onMessageUpdate = (message) => {
        const messageIdHolder = document.getElementById("editedMessageIdHolder");
        messageIdHolder.value = message.id;
        const messageInput = document.getElementsByClassName("message-input-text")[0];
        messageInput.value = message.text;
    }

    onMessageLike = (messageId) => {
        let delta;
        if (this.likedMessagesIds.delete(messageId)) {
            // It means, user already liked message
            delta = -1;
        } else {
            delta = 1;
            this.likedMessagesIds.add(messageId);
        }
        const mapped = this.state.messages.map(m => m.id !== messageId ? m : {
            ...m,
            likeCount: m.likeCount + delta
        });
        this.setState({
            messages: mapped
        });
    }

    onMessageDelete = (messageId) => {
        const filtered = this.state.messages.filter(m => m.id !== messageId);
        this.setState({
            messages: filtered,
            lastMessageDate: filtered[filtered.length - 1]?.createdAt,
            participantsCount: new Set(filtered.map(m => m.userId)).size
        });
    }

    mapResponseEntryToMessageEntity(entry) {
        return new MessageEntity(entry.id, entry.userId, entry.avatar, entry.user,
            entry.text, new Date(entry.createdAt), new Date(entry.editedAt), 0);
    }

    componentDidMount() {
        fetch(this.props.url)
            .then(resp => resp.json())
            .then(data => {
                this.setState({
                    messages: data.map(entry => this.mapResponseEntryToMessageEntity(entry)),
                    lastMessageDate: data[data.length - 1]?.createdAt,
                    participantsCount: new Set(data.map(m => m.userId)).size,
                    isLoading: false
                });
            });
    }

    render() {
        if (this.state.isLoading) {
            return <Preloader/>;
        }
        return (
            <div className="chat">
                <Header
                    chatName="My chat"
                    participantsCount={this.state.participantsCount}
                    messageCount={this.state.messages.length}
                    lastMessageDate={this.state.lastMessageDate}
                />
                <MessageList
                    messages={this.state.messages}
                    likedMessagesIds={this.likedMessagesIds}
                    currentUserId={this.props.currentUserId}
                    onDelete={this.onMessageDelete}
                    onUpdate={this.onMessageUpdate}
                    onLike={this.onMessageLike}
                />
                <MessageInput onMessageSend={this.onMessageSend}/>
            </div>
        );
    }

}

Chat.propTypes = {
    url: PropTypes.string,
    currentUserId: PropTypes.string,
    currentAvatar: PropTypes.string,
    currentUserName: PropTypes.string
}

export default Chat;