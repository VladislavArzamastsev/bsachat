import * as React from 'react';
import './header.css';
import PropTypes from 'prop-types';
import { getFormattedDate } from '../formatter/date-time-formatter';

class Header extends React.Component {

    render() {
        return (
            <div className="header">
                <div className="header-title">
                    {this.props.chatName}
                </div>
                <div className="header-users-count">
                    {this.props.participantsCount}
                </div>
                <div className="header-messages-count">
                    {this.props.messageCount}
                </div>
                <div className="header-last-message-date">
                    {getFormattedDate(this.props.lastMessageDate, "dd.mm.yyyy HH:MM")}
                </div>
            </div>
        );
    }
}

Header.propTypes = {
    chatName: PropTypes.string,
    participantsCount: PropTypes.number,
    messageCount: PropTypes.number,
    lastMessageDate: PropTypes.any
}

export default Header;