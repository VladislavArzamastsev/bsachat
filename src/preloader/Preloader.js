import * as React from 'react';
import "./preloader.css";
import logo from './logo.svg';

class Preloader extends React.Component{

    render() {
        return (
          <div className="preloader">
              <img src={logo} className="App-logo" alt="logo" />
          </div>
        );
    }

}

export default Preloader;