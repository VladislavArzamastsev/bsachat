import * as React from 'react';
import "./message.css";
import PropTypes from 'prop-types';
import { getFormattedDate } from '../formatter/date-time-formatter';
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class OwnMessage extends React.Component {

    render() {
        return (
            <div className="own-message">
                <div className="message-text">
                    {this.props.message.text}
                </div>
                <div className="button-holder">
                    <button className="message-edit" onClick={() => this.props.onUpdate(this.props.message)}>
                        <FontAwesomeIcon icon={faEdit}/>
                    </button>

                    <button className="message-delete" onClick={() => this.props.onDelete(this.props.message.id)}>
                        <FontAwesomeIcon icon={faTrash}/>
                    </button>

                    Likes: {this.props.message.likeCount}
                </div>
                <div className="message-time">
                    {getFormattedDate(this.props.message.createdAt, "HH:MM")}
                </div>
            </div>
        );
    }

}

OwnMessage.propTypes = {
    message: PropTypes.object,
    onDelete: PropTypes.func,
    onUpdate: PropTypes.func
}

export default OwnMessage;