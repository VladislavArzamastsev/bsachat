import * as React from 'react';
import "./message.css";
import PropTypes from 'prop-types';
import {getFormattedDate} from '../formatter/date-time-formatter';
import {faThumbsUp} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class Message extends React.Component {

    render() {
        return (
            <div className={`message ${this.props.liked ? "message-liked" : ""}`}>
                <div className="avatar-and-nickname-holder">
                    <img className="message-user-avatar"
                         src={this.props.message.avatar} alt="Avatar"/>
                    <p className="message-user-name">
                        {this.props.message.user}
                    </p>
                </div>
                <div className="message-text">
                    {this.props.message.text}
                </div>
                <div className="like-holder">
                    {this.props.message.likeCount}
                    <button className="message-like" onClick={() => this.props.onLike(this.props.message.id)}>
                        <FontAwesomeIcon
                            icon={faThumbsUp}
                            className="like-icon"
                        />
                    </button>
                </div>
                <div className="message-time">
                    {getFormattedDate(this.props.message.createdAt, "HH:MM")}
                </div>
            </div>
        );
    }

}

Message.propTypes = {
    message: PropTypes.object,
    onLike: PropTypes.func,
    liked: PropTypes.bool
}

export default Message;